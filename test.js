const assert = require('assert')
const { staticDirectories } = require('.')

assert.ok(Array.isArray(staticDirectories))
assert.ok(staticDirectories.length > 0)
assert.ok(staticDirectories.every((name) => typeof name === 'string'))
