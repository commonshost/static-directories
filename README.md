# @commonshost/static-directories 📁

List of output directories of most static site generators.

These are the default directories into which static sites are generated. This can be useful when detecting the presence of a static site.

## Usage

Simply an array of strings.

```js
const { staticDirectories } =
  require('@commonshost/static-directories')

// [ 'dist', 'public', '_site', ... ]
```

## Caveat

Scan the list in-order. Some frameworks use one directory for output (e.g. `dist`) and another directory for source assets (e.g. `site`). The list is carefully maintained to find the output directory before matching a false-positive source directory.

## Colophon

Made with 💗 by [Sebastiaan Deckers](https://twitter.com/sebdeckers) for [Commons Host](https://commons.host).
