const staticDirectories = [
  // Bankai, Nuxt, React Static, Phenomic, Parcel, Vue, Ember, Angular
  // Grunt, Gulp, Webpack
  'dist',
  // VuePress
  '.vuepress/dist',
  // Hexo, Gatsby, Hugo, Brunch, Harp, Punch
  'public',
  // Jekyll, Exposé, Hakyll
  '_site',
  // Octopress
  '_deploy',
  // Gitbook
  'export',
  // Nanoc, Pelican, Nikola
  'output',
  // Metalsmith, Middleman, Wintersmith
  'build',
  // Cactus
  '.build',
  // MkDocs
  'site',
  // DocPad
  'out',
  // Hyde
  'deploy',
  // Sculpin
  'output_prod', 'output_dev',
  // Polymer 2
  'build/es6-unbundled', 'build/es6-bundled', 'build/es5-bundled',
  // Polymer 1
  'build/unbundled', 'build/bundled',
  // ? (no framework seems to use these)
  'publish', 'public_html',
  // GitHub Pages, Docsify, Docute
  'docs'
]

module.exports.staticDirectories = staticDirectories
